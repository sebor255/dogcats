import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import cv2
from keras.applications.resnet50 import preprocess_input
from keras.applications.imagenet_utils import decode_predictions
from keras.models import load_model
# It can be used to reconstruct the model identically.

currentPath = os.getcwd()
model_dir = "saved_models"
model_weights = "weights_catBreeds4.h5"
model_name = "model_catBreeds4.h5"

model = load_model(os.path.join(currentPath, model_dir, model_name))
model.summary()
model.load_weights(os.path.join(currentPath, model_dir, model_weights))

testFolder = "C:\\Users\\AMD\\Desktop\\testImages"
# filename = "domek.jpg"
filename = "Persian_3.jpg"
# filename = "tuxedo.jpg"
# filename = "Bengal-on-a-backdrop.jpg"
# filename = "bengal2.jpg"
# filename = "white-persian-cats-picture-id637190306.jpg"
# filename = "perski.jpg"
# filename = "Sphynx_25.jpg"
imagefile = os.path.join(testFolder, filename)

input_size = 226
channels = 3

img = cv2.imread(imagefile)
img = cv2.resize(img, (input_size, input_size))
# img_batch = np.expand_dims(img, axis=0)
cv2.imshow("test.jpg", img)

img = np.reshape(img,[1,input_size,input_size,channels])

img_processed = img.astype('float32')

# waits for user to press any key
# (this is necessary to avoid Python kernel form crashing)
cv2.waitKey(0)

# closing all open windows
cv2.destroyAllWindows()


result = model.predict(img_processed)
y_classes = result.argmax(axis=-1)


# print(decode_predictions(result, top=3)[0])
print(result)
print(y_classes)