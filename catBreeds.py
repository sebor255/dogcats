import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.utils import to_categorical

# for plotting images
def plotImages(images_arr):
	fig, axes = plt.subplots(1, 5, figsize=(20, 20))
	axes = axes.flatten()
	for img, ax in zip(images_arr, axes):
		ax.imshow(img)
	plt.tight_layout()
	plt.show()

def getTrainAndValidPaths(pathToFolder: str):
	list_dirs = []
	validation_paths = []
	train_paths = []
	for subdir, dirs, files in os.walk(pathToFolder):
		list_dirs.append(os.path.join(pathToFolder, subdir))
	for x in list_dirs:
		if "validation\\" in x:
			validation_paths.append(x)
		elif "train\\" in x:
			train_paths.append(x)
	assert len(validation_paths) == len(train_paths), "Number of validation paths ({}) and trainpaths ({}) " \
	                                                  "differs".format(len(validation_paths), len(train_paths))
	return train_paths, validation_paths

def countTotalTrainAndVal(train_paths, valid_paths):
	tr = {}
	val = {}
	for vp, tp in zip(valid_paths, train_paths):
		tr[tp[tp.rfind("\\") + 1:]] = len(os.listdir(tp))
		val[vp[vp.rfind("\\") + 1:]] = len(os.listdir(vp))
	total_train = sum(tr.values())
	total_val = sum(val.values())
	return total_train, total_val

def getClassNames(train_paths, valid_paths):
	tr_classes = {}
	val_classes = {}
	for i, x in enumerate(zip(train_paths, valid_paths)):
		tr_classes[i] = x[0][x[0].rfind("\\") + 1:]
		val_classes[i] = x[1][x[1].rfind("\\") + 1:]
		assert tr_classes[i] == val_classes[i], "The class having index number {} in not the same for train {} " \
		                                        "and validation {}".format(i, tr_classes[i], val_classes[i])
	return tr_classes


# getting data
base_dir = "C:\\Users\\AMD\\Desktop\\Python\\Studia\\Uczenie Maszynowe\\dogcats\\catBreeds\\"
train_paths, validation_paths = getTrainAndValidPaths(base_dir)

class_count = len(validation_paths)
print("The number of classes to analyze: ", class_count)

# now we need to get names of classes:
classNames_dict = getClassNames(train_paths, validation_paths)
print(classNames_dict)
classNames_list = [x for x in classNames_dict.values()]
print(classNames_list)

# and prepare the categorical vectors of the dimentions len(classNames) x len(classNames) each row like:  [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
# when the data generator is used we actually do not need this cause the categorical classes will be extracted from
# the data generator. Here, only for visualization as we use data generator.
categorical_classes = to_categorical(range(len(classNames_dict)))
print(categorical_classes)

BATCH_SIZE = 32  # how many items are read at once
IMG_SIZE = 226  # one dimention, we want to have square image

# generators
train_image_generator = ImageDataGenerator(
	rescale=1. / 255,
	width_shift_range=0.2,
	height_shift_range=0.2,
	shear_range=0.2,
	zoom_range=0.2,
	horizontal_flip=True,
	fill_mode='nearest'  # fills missing pixels when we do transformations

)
validation_image_generator = ImageDataGenerator(
	rescale=1. / 255)

train_data_gen = train_image_generator.flow_from_directory(
	directory= os.path.join(base_dir, "train"),
	target_size=(IMG_SIZE, IMG_SIZE),
	color_mode="rgb",
	classes=classNames_list,
	class_mode="categorical",
	batch_size=BATCH_SIZE,
	shuffle=True,
)

print(train_data_gen)
# batch_size: No. of images to be yielded from the generator per batch.
# class_mode: Set “binary” if you have only two classes to predict, if not set to “categorical”


val_data_gen = train_image_generator.flow_from_directory(
	directory=os.path.join(base_dir, "validation"),
	target_size=(IMG_SIZE, IMG_SIZE),
	color_mode="rgb",
	classes=classNames_list,
	class_mode="categorical",
	batch_size=BATCH_SIZE,
	shuffle=False,
)
print(train_data_gen)
# images = plotImages([train_data_gen[0][0][0] for i in range(5)])
# plotImages(images)

# model preperation
channels = 3  # RGB color image
# channels = 1          # Grayscale image


model = tf.keras.models.Sequential([
	# convolution of each layer
	# tf.keras.Input(shape=(IMG_SIZE,IMG_SIZE,3)),
	# tf.keras.layers.experimental.preprocessing.RandomRotation(factor=0.3),  # 0.3 * 2pi -> 108 stopni
	# tf.keras.layers.experimental.preprocessing.RandomZoom(height_factor=(0.0, 0.4)),
	# tf.keras.layers.experimental.preprocessing.RandomFlip(mode="horizontal"),

	# stride works as polling - downscales the image
	tf.keras.layers.Conv2D(16, (7, 7), activation='relu', input_shape=(IMG_SIZE, IMG_SIZE, channels), strides=2),

	tf.keras.layers.Conv2D(32, (3, 3), activation='relu'),
	tf.keras.layers.Conv2D(32, (3, 3), activation='relu', strides=2),

	tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
	tf.keras.layers.Conv2D(64, (3, 3), activation='relu', strides=2),
	tf.keras.layers.MaxPooling2D((2, 2)),
	tf.keras.layers.Dropout(0.5),       ##Optimalization part of neurons will be turned off randomly

	# tf.keras.layers.Dropout(0.5),  ##Optimalization part of neurons will be turned off randomly

	# tf.keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=(IMG_SIZE, IMG_SIZE, channels), strides=2),

	# tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
	# tf.keras.layers.Conv2D(64, (3, 3), activation='relu', strides=2),

	# tf.keras.layers.Conv2D(128, (3, 3), activation='relu'),
	# tf.keras.layers.Conv2D(128, (3, 3), activation='relu', strides=2),

	# tf.keras.layers.Conv2D(256, (3, 3), activation='relu'),
	# tf.keras.layers.Conv2D(256, (3, 3), activation='relu', strides=2),
	#
	# tf.keras.layers.Conv2D(512, (3, 3), activation='relu'),
	# tf.keras.layers.Dropout(0.5),       ##Optimalization part of neurons will be turned off randomly

	# now flattening to mage 1 dimension out of 3 (width, hight, channels)
	tf.keras.layers.Flatten(),
	tf.keras.layers.Dense(512, activation='relu'),

	# tf.keras.layers.Dense(128, activation='relu'),

	# now the time for the output layer:
	tf.keras.layers.Dense(class_count, activation='softmax')  # y = [0, 1] or [1, 0]
])

# model.compile(optimizer="adam",
#               loss="categorical_crossentropy",
#               metrics=["accuracy"])
#
# model.summary()

######## adding dataAugmentation layer ##########
#cropScale = int(0.8*IMG_SIZE)
data_augmentation = tf.keras.models.Sequential([
	# tf.keras.layers.experimental.preprocessing.RandomCrop(height=cropScale,width=cropScale,),


])

# inputs = tf.keras.Input(shape=(IMG_SIZE,IMG_SIZE,3))
# # x = data_augmentation(inputs)
# # model = model(x)

model.compile(optimizer="adam",
              loss="categorical_crossentropy",
              metrics=["accuracy"])

model.summary()

EPOCHS = 50

currentPath = os.getcwd()
my_callbacks = [
    tf.keras.callbacks.EarlyStopping(patience=2),
    tf.keras.callbacks.ModelCheckpoint(filepath=os.path.join(currentPath,'model.{epoch:02d}-{val_loss:.2f}.h5')),
    tf.keras.callbacks.TensorBoard(log_dir='./logs'),
]


STEP_SIZE_TRAIN=train_data_gen.n//train_data_gen.batch_size
STEP_SIZE_VALID=val_data_gen.n//val_data_gen.batch_size

history = model.fit_generator(
	train_data_gen,
	steps_per_epoch=STEP_SIZE_TRAIN,
	epochs=EPOCHS,
	validation_data=val_data_gen,
	validation_steps=STEP_SIZE_VALID,
)

# analysis
acc = history.history["accuracy"]
val_acc = history.history["val_accuracy"]

loss = history.history["loss"]
val_loss = history.history["val_loss"]

epochs_range = range(EPOCHS)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label="Validation Accuracy")
plt.legend(loc="lower right")
plt.title("Training and Validation Accuracy")

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label="Training Loss")
plt.plot(epochs_range, val_loss, label="Validation Loss")
plt.legend(loc="upper right")
plt.title("Training and Validation Loss")
plt.show()

# model.save('cat_breeds_model')

# # serialize model to JSON
# model_json = model.to_json()
# with open("myModel_cat_breeds.json", "w") as json_file:
# 	json_file.write(model_json)
# # serialize weights to HDF5
# model.save_weights("myModel_cat_breeds.h5")
# print("Saved model to disk")



model_dir = "saved_models"
model_weights = "weights_catBreeds7.h5"
model_name = "model_catBreeds7.h5"
# here we can saved our trained weights to be able to reuse our model in the future
model.save_weights(
	filepath=os.path.join(currentPath, model_dir, model_weights),
	save_format='h5'
)

# we can also save the whole model:
tf.keras.models.save_model(
	model=model,
	filepath=os.path.join(currentPath, model_dir, model_name),
	save_format='h5'
)