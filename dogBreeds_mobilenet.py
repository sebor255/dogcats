import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflow_hub as hub
from tensorflow.keras.utils import to_categorical


# for plotting images
def plotImages(images_arr):
	fig, axes = plt.subplots(1, 5, figsize=(20, 20))
	axes = axes.flatten()
	for img, ax in zip(images_arr, axes):
		ax.imshow(img)
	plt.tight_layout()
	plt.show()

def getTrainAndValidPaths(pathToFolder: str):
	list_dirs = []
	validation_paths = []
	train_paths = []
	for subdir, dirs, files in os.walk(pathToFolder):
		list_dirs.append(os.path.join(pathToFolder, subdir))
	for x in list_dirs:
		if "validation\\" in x:
			validation_paths.append(x)
		elif "train\\" in x:
			train_paths.append(x)
	assert len(validation_paths) == len(train_paths), "Number of validation paths ({}) and trainpaths ({}) " \
	                                                  "differs".format(len(validation_paths), len(train_paths))
	return train_paths, validation_paths

def countTotalTrainAndVal(train_paths, valid_paths):
	tr = {}
	val = {}
	for vp, tp in zip(valid_paths, train_paths):
		tr[tp[tp.rfind("\\") + 1:]] = len(os.listdir(tp))
		val[vp[vp.rfind("\\") + 1:]] = len(os.listdir(vp))
	total_train = sum(tr.values())
	total_val = sum(val.values())
	return total_train, total_val

def getClassNames(train_paths, valid_paths):
	tr_classes = {}
	val_classes = {}
	for i, x in enumerate(zip(train_paths, valid_paths)):
		tr_classes[i] = x[0][x[0].rfind("\\") + 1:]
		val_classes[i] = x[1][x[1].rfind("\\") + 1:]
		assert tr_classes[i] == val_classes[i], "The class having index number {} in not the same for train {} " \
		                                        "and validation {}".format(i, tr_classes[i], val_classes[i])
	return tr_classes




# getting data
base_dir = "C:\\Users\\AMD\\Desktop\\Python\\Studia\\Uczenie Maszynowe\\dogcats\\catBreeds\\"
train_paths, validation_paths = getTrainAndValidPaths(base_dir)

class_count = len(validation_paths)
print("The number of classes to analyze: ", class_count)

# now we need to get names of classes:
classNames_dict = getClassNames(train_paths, validation_paths)
print(classNames_dict)
classNames_list = [x for x in classNames_dict.values()]
print(classNames_list)

# and prepare the categorical vectors of the dimentions len(classNames) x len(classNames) each row like:  [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
# when the data generator is used we actually do not need this cause the categorical classes will be extracted from
# the data generator. Here, only for visualization as we use data generator.
categorical_classes = to_categorical(range(len(classNames_dict)))
print(categorical_classes)

# total_train = sum(num_cats_breeds_tr.values())
# total_val = sum(num_cats_breeds_val.values())
# total_train = num_cats_tr + num_dogs_tr
# total_val = num_cats_val + num_dogs_val

BATCH_SIZE = 32  # how many items are read at once
IMG_SIZE = 224  # one dimention, we want to have square image

# generators
train_image_generator = ImageDataGenerator(
	rescale=1. / 255,
	width_shift_range=0.2,
	height_shift_range=0.2,
	shear_range=0.2,
	zoom_range=0.2,
	horizontal_flip=True,
	fill_mode='nearest'  # fills missing pixels when we do transformations

)
validation_image_generator = ImageDataGenerator(
	rescale=1. / 255)

train_data_gen = train_image_generator.flow_from_directory(batch_size=BATCH_SIZE,
                                                           directory= os.path.join(base_dir, "train"),
                                                           shuffle=True,
															color_mode="rgb",
                                                           target_size=(IMG_SIZE, IMG_SIZE),
                                                           class_mode='categorical')

# batch_size: No. of images to be yielded from the generator per batch.
# class_mode: Set “binary” if you have only two classes to predict, if not set to “categorical”
label_map = (train_data_gen.class_indices)


val_data_gen = train_image_generator.flow_from_directory(batch_size=BATCH_SIZE,
                                                         directory=os.path.join(base_dir, "validation"),
                                                         shuffle=False,
													     color_mode="rgb",
                                                         target_size=(IMG_SIZE, IMG_SIZE),
                                                         class_mode='categorical')

# images = plotImages([train_data_gen[0][0][0] for i in range(5)])
# plotImages(images)


# model preperation
channels = 3  # RGB color image
# channels = 1          # Grayscale image

# mobile = tf.keras.applications.mobilenet.MobileNet()
# x = mobile.layers[-6].output
# output = tf.keras.layers.Dense(units=class_count, activation="softmax")(x)
#
# model = tf.keras.models.Model(inputs=mobile.input, outputs=output)
#
# for layer in model.layers[:-23]:
# 	layer.trainable = False


# getting MobileNet
URL = "https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4"
mobile_net = hub.KerasLayer(URL, input_shape=(IMG_SIZE, IMG_SIZE, channels))
mobile_net.trainable = False

model = tf.keras.models.Sequential([

	# this won't be trained
	mobile_net,
	# only the last layer will be trained
	tf.keras.layers.Dense(class_count, activation='softmax')  # y = [0, 1] or [1, 0]

])

model.compile(optimizer="adam",
              loss="categorical_crossentropy",
              metrics=["accuracy"])

model.summary()

EPOCHS = 100

STEP_SIZE_TRAIN=train_data_gen.n//train_data_gen.batch_size
STEP_SIZE_VALID=val_data_gen.n//val_data_gen.batch_size

history = model.fit_generator(
	train_data_gen,
	steps_per_epoch=STEP_SIZE_TRAIN,
	epochs=EPOCHS,
	validation_data=val_data_gen,
	validation_steps=STEP_SIZE_VALID,
)

# analysis
acc = history.history["accuracy"]
val_acc = history.history["val_accuracy"]

loss = history.history["loss"]
val_loss = history.history["val_loss"]

epochs_range = range(EPOCHS)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label="Validation Accuracy")
plt.legend(loc="lower right")
plt.title("Training and Validation Accuracy")

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label="Training Loss")
plt.plot(epochs_range, val_loss, label="Validation Loss")
plt.legend(loc="upper right")
plt.title("Training and Validation Loss")
plt.show()

# model.save('cat_breeds_model')

# # serialize model to JSON
# model_json = model.to_json()
# with open("myModel_cat_breeds.json", "w") as json_file:
# 	json_file.write(model_json)
# # serialize weights to HDF5
# model.save_weights("myModel_cat_breeds.h5")
# print("Saved model to disk")

currentPath = os.getcwd()
model_dir = "saved_models"
model_weights = "weights_mobilenetCatBreeds.h5"
model_name = "model_mobilenetCatBreeds.h5"
# here we can saved our trained weights to be able to reuse our model in the future
model.save_weights(
	filepath=os.path.join(currentPath, model_dir, model_weights),
	save_format='h5'
)

# we can also save the whole model:
tf.keras.models.save_model(
	model=model,
	filepath=os.path.join(currentPath, model_dir, model_name),
	save_format='h5')