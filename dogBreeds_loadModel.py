import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import cv2
from keras.applications.resnet50 import preprocess_input
from keras.applications.imagenet_utils import decode_predictions
from keras.models import load_model
import tensorflow_hub as hub
# It can be used to reconstruct the model identically.

ownModel = False

if not ownModel:
	model_location = r"C:\Users\AMD\Desktop\Python\Studia\Uczenie Maszynowe\dogcats\mobilenetModel_dogBreeds"
	# when the model is mobilenet use this:
	loaded = tf.saved_model.load(model_location)
	infer = loaded.signatures["serving_default"]
	print(infer.structured_outputs)
	print("MobileNet has {} trainable variables: {}, ...".format(
		len(loaded.trainable_variables),
		", ".join([v.name for v in loaded.trainable_variables[:5]])))


else:
	# when the model is our own use this:
	model_location = "mobilenetModel_dog_breeds.h5"
	model = load_model(model_location, custom_objects={'KerasLayer': hub.KerasLayer})
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	model.summary()

path = "C:\\Users\\AMD\\Desktop\\"
#filename = "domek.jpg"
# filename = "beagle.jpg"
filename = "Taka_Shiba.jpg"

input_size = 224
channels = 3

img = cv2.imread(path + filename)
img = cv2.resize(img, (input_size, input_size))
# img_batch = np.expand_dims(img, axis=0)
img = np.reshape(img,[1,input_size,input_size,channels])
# cv2.imshow("test.jpg", img[0])
img_processed = img.astype('float32')

# waits for user to press any key
# (this is necessary to avoid Python kernel form crashing)
cv2.waitKey(0)

# closing all open windows
cv2.destroyAllWindows()


result = loaded.predict(img_processed, batch_size=1)
y_classes = result.argmax(axis=-1)


# print(decode_predictions(result, top=3)[0])
print(result)
print(y_classes)