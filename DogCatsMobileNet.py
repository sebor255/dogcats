# This is pretty complex neural network example which will allow us to get better accuracy than in the DogCats.py example

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import os 
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflow_hub as hub

# for plotting images 
def plotImages(images_arr):
    fig, axes = plt.subplots(1, 5, figsize=(20, 20))
    axes  = axes.flatten()
    for img, ax in zip(images_arr, axes):
        ax.imshow(img)
    plt.tight_layout()
    plt.show()


# getting data
base_dir = "C:\\Users\\sebrog\\Desktop\\SCRIPTS\\ML\\DogsCats\\cats_and_dogs_filtered\\"
train_dir = os.path.join(base_dir, 'train')
validation_dir = os.path.join(base_dir, 'validation')

train_cats = os.path.join(train_dir, 'cats')
train_dogs = os.path.join(train_dir, 'dogs')
validation_cats = os.path.join(validation_dir, 'cats')
validation_dogs = os.path.join(validation_dir, 'dogs')

num_cats_tr = len(os.listdir(train_cats))
num_dogs_tr = len(os.listdir(train_dogs))
num_cats_val = len(os.listdir(validation_cats))
num_dogs_val = len(os.listdir(validation_dogs))

total_train = num_cats_tr + num_dogs_tr
total_val = num_cats_val + num_dogs_val

BATCH_SIZE = 32
IMG_SIZE = 224          # one dimention, we want to have square image ---> 224 matches the mobile network input 


# generators
train_image_generator = ImageDataGenerator(
    rescale = 1./255,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest'         # fills missing pixels when we do transformations

)
validation_image_generator = ImageDataGenerator(
    rescale=1./255)

train_data_gen = train_image_generator.flow_from_directory(batch_size=BATCH_SIZE,
                                                            directory=train_dir,
                                                            shuffle=True,
                                                            target_size=(IMG_SIZE,IMG_SIZE),
                                                            class_mode='binary')

# batch_size: No. of images to be yielded from the generator per batch.
# class_mode: Set “binary” if you have only two classes to predict, if not set to “categorical”


val_data_gen = train_image_generator.flow_from_directory(batch_size=BATCH_SIZE,
                                                            directory=validation_dir,
                                                            shuffle=False,
                                                            target_size=(IMG_SIZE,IMG_SIZE),
                                                            class_mode='binary')

# images = plotImages([train_data_gen[0][0][0] for i in range(5)])
# plotImages(images)

# model preperation
channels = 3        # RGB color image
# channels = 1        # Grayscale image

# getting MobileNet
URL = "https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4"
mobile_net = hub.KerasLayer(URL, input_shape=(IMG_SIZE, IMG_SIZE, channels))
mobile_net.trainable = False

model = tf.keras.models.Sequential([
    # this won't be trained
    mobile_net,
    # only the last layer will be trained
    tf.keras.layers.Dense(2, activation='softmax')  # y = [0, 1] or [1, 0]

])

# adam is most popular optimizer algorythm
model.compile(optimizer="adam",
                loss="sparse_categorical_crossentropy",
                metrics=["accuracy"])

model.summary()

EPOCHS = 15

history = model.fit_generator(
    train_data_gen,
    steps_per_epoch=int(np.ceil(total_train / float(BATCH_SIZE))),
    epochs=EPOCHS,
    validation_data=val_data_gen,
    validation_steps=int(np.ceil(total_val / float(BATCH_SIZE)))
)

# analysis 
acc = history.history["accuracy"]
val_acc = history.history["val_accuracy"]

loss = history.history["loss"]
val_loss = history.history["val_loss"]

epochs_range = range(EPOCHS)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label="Validation Accuracy")
plt.legend(loc="lower right")
plt.title("Training and Validation Accuracy")

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label="Training Loss")
plt.plot(epochs_range, val_loss, label="Validation Loss")
plt.legend(loc="upper right")
plt.title("Training and Validation Loss")
plt.show()