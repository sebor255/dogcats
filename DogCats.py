import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import os 
from tensorflow.keras.preprocessing.image import ImageDataGenerator


# for plotting images 
def plotImages(images_arr):
    fig, axes = plt.subplots(1, 5, figsize=(20, 20))
    axes  = axes.flatten()
    for img, ax in zip(images_arr, axes):
        ax.imshow(img)
    plt.tight_layout()
    plt.show()


# getting data
actual_path = os.getcwd()
base_dir = os.path.join(actual_path,'cats_and_dogs_filtered')
train_dir = os.path.join(base_dir, 'train')
validation_dir = os.path.join(base_dir, 'validation')

train_cats = os.path.join(train_dir, 'cats')
train_dogs = os.path.join(train_dir, 'dogs')
validation_cats = os.path.join(validation_dir, 'cats')
validation_dogs = os.path.join(validation_dir, 'dogs')

num_cats_tr = len(os.listdir(train_cats))
num_dogs_tr = len(os.listdir(train_dogs))
num_cats_val = len(os.listdir(validation_cats))
num_dogs_val = len(os.listdir(validation_dogs))

total_train = num_cats_tr + num_dogs_tr
total_val = num_cats_val + num_dogs_val

BATCH_SIZE = 32             # how many items are read at once
IMG_SIZE = 150              # one dimention, we want to have square image


# generators
train_image_generator = ImageDataGenerator(
    rescale = 1./255,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest'         # fills missing pixels when we do transformations

)
validation_image_generator = ImageDataGenerator(
    rescale=1./255)

train_data_gen = train_image_generator.flow_from_directory(batch_size=BATCH_SIZE,
                                                            directory=train_dir,
                                                            shuffle=True,
                                                            target_size=(IMG_SIZE,IMG_SIZE),
                                                            class_mode='binary')

# batch_size: No. of images to be yielded from the generator per batch.
# class_mode: Set “binary” if you have only two classes to predict, if not set to “categorical”


val_data_gen = train_image_generator.flow_from_directory(batch_size=BATCH_SIZE,
                                                            directory=validation_dir,
                                                            shuffle=False,
                                                            target_size=(IMG_SIZE,IMG_SIZE),
                                                            class_mode='binary')


# images = plotImages([train_data_gen[0][0][0] for i in range(5)])
# plotImages(images)


# model preperation
channels = 3            # RGB color image
# channels = 1          # Grayscale image
model = tf.keras.models.Sequential([

    # tf.keras.Input(shape=(IMG_SIZE,IMG_SIZE,3)),
	# tf.keras.layers.experimental.preprocessing.RandomRotation(factor=0.3),  # 0.3 * 2pi -> 108 stopni
	# tf.keras.layers.experimental.preprocessing.RandomZoom(height_factor=(0.0, 0.4)),
	# tf.keras.layers.experimental.preprocessing.RandomFlip(mode="horizontal"),

    tf.keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=(IMG_SIZE, IMG_SIZE, channels)),
    tf.keras.layers.MaxPooling2D(2, 2),

    tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),

    tf.keras.layers.Conv2D(128, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),

    tf.keras.layers.Conv2D(128, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),

    tf.keras.layers.Dropout(0.3),   ##Optimalization part of neurons will be turned off randomly


    # tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    # tf.keras.layers.MaxPooling2D(2, 2),

    # tf.keras.layers.Conv2D(128, (3, 3), activation='relu'),
    # tf.keras.layers.MaxPooling2D(2, 2),

    # tf.keras.layers.Dropout(0.5),   ##Optimalization part: 1/2 of neurons will be turned off randomly
    # # to ensure the model is more robust

    # now flattening to mage 1 dimension out of 3 (width, hight, channels)

    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(256, activation='relu'),

    # now the time for the output layer:

    tf.keras.layers.Dense(2, activation='softmax')  # y = [0, 1] or [1, 0]

])


model.compile(optimizer="adam",
                loss="sparse_categorical_crossentropy",
                metrics=["accuracy"])

model.summary()

EPOCHS = 100

history = model.fit_generator(
    train_data_gen,
    steps_per_epoch=int(np.ceil(total_train / float(BATCH_SIZE))),
    epochs=EPOCHS,
    validation_data=val_data_gen,
    validation_steps=int(np.ceil(total_val / float(BATCH_SIZE)))
)

# analysis 
acc = history.history["accuracy"]
val_acc = history.history["val_accuracy"]

loss = history.history["loss"]
val_loss = history.history["val_loss"]

epochs_range = range(EPOCHS)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label="Validation Accuracy")
plt.legend(loc="lower right")
plt.title("Training and Validation Accuracy")

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label="Training Loss")
plt.plot(epochs_range, val_loss, label="Validation Loss")
plt.legend(loc="upper right")
plt.title("Training and Validation Loss")
plt.show()

model_dir = "saved_models"
model_weights = "weights_catsDogs.h5"
model_name = "model_catDogs.h5"
# here we can saved our trained weights to be able to reuse our model in the future
model.save_weights(
	filepath=os.path.join(actual_path, model_dir, model_weights),
	save_format='h5'
)

# we can also save the whole model:
tf.keras.models.save_model(
	model=model,
	filepath=os.path.join(actual_path, model_dir, model_name),
	save_format='h5'
)