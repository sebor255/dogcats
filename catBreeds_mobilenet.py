import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflow_hub as hub

# config = tf.ConfigProto()
# config.gpu_options.allow_growth = True
# session = tf.Session(config=config, ...)

# for plotting images
def plotImages(images_arr):
	fig, axes = plt.subplots(1, 5, figsize=(20, 20))
	axes = axes.flatten()
	for img, ax in zip(images_arr, axes):
		ax.imshow(img)
	plt.tight_layout()
	plt.show()


# getting data
base_dir = "C:\\Users\\AMD\\Desktop\\Python\\Studia\\Uczenie Maszynowe\\dogcats\\catBreeds\\"

list_dirs = []
for subdir, dirs, files in os.walk(base_dir):
    list_dirs.append(os.path.join(base_dir, subdir))

print(list_dirs)

validation_paths = []
train_paths = []
for x in list_dirs:
	if "validation\\" in x:
		validation_paths.append(x)
	elif "train\\" in x:
		train_paths.append(x)

class_count = len(validation_paths)
print("The number of classes to analyze: ", class_count)

num_cats_breeds_tr = {}
num_cats_breeds_val = {}


for vp, tp in zip(validation_paths, train_paths):
	num_cats_breeds_val[vp[vp.rfind("\\") + 1:]] = len(os.listdir(vp))
	num_cats_breeds_tr[tp[tp.rfind("\\") + 1:]] = len(os.listdir(tp))

total_train = sum(num_cats_breeds_tr.values())
total_val = sum(num_cats_breeds_val.values())
# total_train = num_cats_tr + num_dogs_tr
# total_val = num_cats_val + num_dogs_val

BATCH_SIZE = 32  # how many items are read at once
IMG_SIZE = 224  # one dimention, we want to have square image

# generators
train_image_generator = ImageDataGenerator(
	rescale=1. / 255,
	width_shift_range=0.2,
	height_shift_range=0.2,
	shear_range=0.2,
	zoom_range=0.2,
	horizontal_flip=True,
	fill_mode='nearest'  # fills missing pixels when we do transformations

)
validation_image_generator = ImageDataGenerator(
	rescale=1. / 255)

train_data_gen = train_image_generator.flow_from_directory(batch_size=BATCH_SIZE,
                                                           directory= os.path.join(base_dir, "train"),
                                                           shuffle=True,
															color_mode="rgb",
                                                           target_size=(IMG_SIZE, IMG_SIZE),
                                                           class_mode='categorical')

# batch_size: No. of images to be yielded from the generator per batch.
# class_mode: Set “binary” if you have only two classes to predict, if not set to “categorical”


val_data_gen = train_image_generator.flow_from_directory(batch_size=BATCH_SIZE,
                                                         directory=os.path.join(base_dir, "validation"),
                                                         shuffle=False,
													     color_mode="rgb",
                                                         target_size=(IMG_SIZE, IMG_SIZE),
                                                         class_mode='categorical')

# images = plotImages([train_data_gen[0][0][0] for i in range(5)])
# plotImages(images)


# model preperation
channels = 3  # RGB color image
# channels = 1          # Grayscale image

# getting MobileNet
URL = "https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4"
mobile_net = hub.KerasLayer(URL, input_shape=(IMG_SIZE, IMG_SIZE, channels))
mobile_net.trainable = False



model = tf.keras.models.Sequential([

	# this won't be trained
	mobile_net,
	# only the last layer will be trained
	tf.keras.layers.Dense(class_count, activation='softmax')  # y = [0, 1] or [1, 0]

])

model.compile(optimizer="adam",
              loss="categorical_crossentropy",
              metrics=["accuracy"])

model.summary()

EPOCHS = 150

STEP_SIZE_TRAIN=train_data_gen.n//train_data_gen.batch_size
STEP_SIZE_VALID=val_data_gen.n//val_data_gen.batch_size

history = model.fit_generator(
	train_data_gen,
	steps_per_epoch=STEP_SIZE_TRAIN,
	epochs=EPOCHS,
	validation_data=val_data_gen,
	validation_steps=STEP_SIZE_VALID,
)

# analysis
acc = history.history["accuracy"]
val_acc = history.history["val_accuracy"]

loss = history.history["loss"]
val_loss = history.history["val_loss"]

epochs_range = range(EPOCHS)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label="Validation Accuracy")
plt.legend(loc="lower right")
plt.title("Training and Validation Accuracy")

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label="Training Loss")
plt.plot(epochs_range, val_loss, label="Validation Loss")
plt.legend(loc="upper right")
plt.title("Training and Validation Loss")
plt.show()

model.save("mobilenetModel_cat_breeds.h5")